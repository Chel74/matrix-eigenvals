from typing import List

from sympy import Matrix

import random
import math

matrix1 = [[2, -18, 1, 1, 2, 1, 0, 0],
           [1, -32, 2, 2, 1, 0, 1, 2],
           [1, -8, 0, 0, 0, 0, 2, 0],
           [1, -30, 0, 2, 2, 2, 2, 0],
           [2, -14, 0, 1, 2, 1, 1, 0],
           [2, -11, 2, 0, 0, 2, 0, 1],
           [2, -9, 2, 2, 1, 0, 0, 1],
           [2, -17, 1, 1, 1, 2, 2, 0]]

matrix2 = [[1, 2, 3],
           [4, 5, 6],
           [7, 8, 9]]


def matmul(m1, m2, ret_matrix=False):
    s = 0  # сумма
    t = []  # временная матрица
    m3 = []  # конечная матрица
    if len(m2) != len(m1[0]):
        ValueError("Матрицы не могут быть перемножены")
    else:
        r1 = len(m1)
        c1 = len(m1[0])
        c2 = len(m2[0])
        for z in range(0, r1):
            for j in range(0, c2):
                for i in range(0, c1):
                    s = s + m1[z][i] * m2[i][j]
                t.append(s)
                s = 0
            m3.append(t)
            t = []
    if ret_matrix:
        return MyMatrix(len(m3), len(m3[0]), m3)
    else:
        return m3


def negate_matrix(a):
    for i in range(len(a)):
        for j in range(len(a[0])):
            a[i][j] = -1 * a[i][j]
    return a


def get_toeplitz_matrix(rows, cols, diags):
    def entry(i, j):
        if j > i:
            return 0
        return diags[i - j]

    m = []
    for i in range(rows):
        m.append([entry(i, j) for j in range(cols)])
    return m


def bound(coefficients):
    coefficients.reverse()
    n = len(coefficients) - 1
    b = 0.0
    for i in range(n):
        b += abs(coefficients[i] / coefficients[i + 1])
    coefficients.reverse()
    return b

def polynomial(z, coefficients): # Horner scheme
    t = complex(0, 0)
    for c in coefficients:
        t = t * z + c
    return t


def make_complex(a):
    comp = []
    for i in a:
        comp.append(complex(i, 0))
    return comp


eps = 1e-7 # max error allowed
def DurandKerner(coefficients):
    n = len(coefficients) - 1
    roots = [complex(0, 0)] * n
    bnd = bound(coefficients)
    retry = True
    while retry:
        retry = False
        # set initial roots as random points within bounding circle
        for k in range(n):
            r = bnd * random.random()
            theta = 2.0 * math.pi * random.random()
            roots[k] = complex(r * math.cos(theta), r * math.sin(theta))

        itCtr = 0
        rootsNew = roots[:]
        flag = True
        while flag:
            flag = False
            for k in range(n):
                temp = complex(1.0, 0.0)
                for j in range(n):
                    if j != k:
                        temp *= roots[k] - roots[j]
                rootsNew[k] = roots[k] - polynomial(roots[k], coefficients) / temp
                if abs(roots[k] - rootsNew[k]) > eps:
                    # print abs(roots[k] - rootsNew[k])
                    flag = True
                if math.isnan(rootsNew[k].real) or math.isnan(rootsNew[k].imag):
                    flag = False
                    retry = True
                    print ('retrying...')
                    break
            roots = rootsNew[:]
            itCtr += 1

    return roots

#TODO Добавил для проверки
def flat_list(a):
    flat = []
    for sublist in a:
        for item in sublist:
            flat.append( item )
    return flat


class MyMatrix:

    def __init__(self, rows: int, cols: int, m: List[List[int]]):
        self.matrix = m
        self.rows = rows
        self.cols = cols

    def _eval_berkowitz_toeplitz_matrix(self):
        if self.rows == 0 and self.cols == 0:
            return self._new(1, 1, [1])

        a = self.matrix[0][0]
        R = [self.matrix[0][1:]]
        C = [[i[0]] for i in self.matrix[1:]]
        A = [i[1:] for i in self.matrix[1:]]

        diags = [C]
        for i in range(self.rows - 2):
            diags.append(matmul(A, diags[i]))

        negate_R = negate_matrix(R)
        diags = [matmul(negate_R, d)[0][0] for d in diags]
        diags = [1, -a] + diags

        toeplitz = self._new(self.cols + 1, self.rows, get_toeplitz_matrix(self.cols + 1, self.rows, diags))
        return self._new(len(A), len(A[0]), A), toeplitz

    def _eval_berkowitz_vector(self):

        if self.rows == 0 and self.cols == 0:
            return self._new(1, 1, [1])
        elif self.rows == 1 and self.cols == 1:
            return self._new(2, 1, [[1], [-1 * self.matrix[0][0]]])

        submat, toeplitz = self._eval_berkowitz_toeplitz_matrix()
        return matmul(toeplitz.matrix, submat._eval_berkowitz_vector().matrix, ret_matrix=True)

    def charpoly(self):
        berk_vector = self._eval_berkowitz_vector()
        return berk_vector.matrix

    @classmethod
    def _new(cls, r, c, m):
        return cls(r, c, m)


if __name__ == '__main__':
    matrix = matrix2
    M = Matrix(matrix)

    m_matrix = MyMatrix(len(matrix), len(matrix[0]), matrix)
    print(m_matrix.charpoly())
    print("roots: " + str(DurandKerner( make_complex(flat_list(m_matrix.charpoly()))) ) )

    #print(np.roots(flat_list(m_matrix.charpoly())))

    # coefficients = [complex( 1, 0 ), complex( -15, 0 ), complex( -18, 0 ), complex( 0, 0 )]
    # print ("coefficients: " + str( coefficients ))
    # print ("roots: " + str( DurandKerner( coefficients ) ))

    # Comment from Vlad
    #TODO найти корни (roots) используя коэффициенты

    print(M.charpoly(x='λ'))
    print(M.eigenvals(error_when_incomplete=False, multiple=True))
